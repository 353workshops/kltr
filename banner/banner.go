package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func main() {
	banner("Go", 6)
	banner("G☺", 6)

	s := "G☺!"
	fmt.Println("len:", len(s)) // bytes
	b := s[0]                   // b is a byte (uint8)
	fmt.Println(b)
	fmt.Printf("%c\n", b)

	for i, c := range s {
		// c is a "rune" (int32)
		fmt.Printf("%d: %c\n", i, c)
	}

	// s[0] = 'g' // won't compile, strings are immutable
	fmt.Println(strings.ToUpper("hello"))

	fmt.Printf("%-4s!\n", "Go")

	x, y := 1, "1"
	fmt.Printf("x=%v, y=%v\n", x, y)
	fmt.Printf("x=%#v, y=%#v\n", x, y)
}

// banner("Go", 6)
//
//  Go
//-------

func banner(text string, width int) {
	// padding := (width - len(text)) / 2
	padding := (width - utf8.RuneCountInString(text)) / 2
	for i := 0; i < padding; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)

	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}
