package main

import "fmt"

func main() {
	var i Item
	fmt.Println("i:", i)
	fmt.Printf(" v: %v\n", i)
	fmt.Printf("+v: %+v\n", i)
	fmt.Printf("#v: %#v\n", i) // Use for logs & debug prints

	i = Item{10, 20} // Must provide all fields
	fmt.Printf("i: %#v\n", i)

	i = Item{ // Can do any order, not all fields
		Y: 50, // dangling comma is required
		//		X: 3,
	}
	fmt.Printf("i: %#v\n", i)
	i.X = 30
	fmt.Printf("i: %#v\n", i)

	fmt.Println(NewItem(100, 200))
	fmt.Println(NewItem(100, -200))

	i.Move(170, 36) // Works even though Move has pointer receiver
	fmt.Printf("i: %#v\n", i)

	p1 := Player{
		Name: "Parzival",
		//		Item: Item{10, 20},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Printf("p1.X: %#v\n", p1.X)
	fmt.Printf("p1.Item.X: %#v\n", p1.Item.X)
	p1.Move(400, 500)
	fmt.Printf("p1: %#v\n", p1)

	fmt.Println(p1.Found(Key(12))) // error
	fmt.Println(p1.Found(Copper))  // <nil>
	fmt.Println(p1.Found(Copper))  // <nil>
	fmt.Println(p1.Keys)           // [copper]

	ms := []Mover{
		&i,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Println(m)
	}
}

/* Thought experiments

Go:
type Reader interface{
	Read(p []byte) (int, error)
}

Python:
type Reader interface {
	Read(n int) ([]byte, error)
}

Sorting interface:

type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

*/

// Validate that Player implements Mover
// var _ Mover = &Player{}

// interfaces say what we need (not what we provide)
// interfaces are small (stdlib avg < 2, if you have more than 5 - rethink)
// idiom: return types, accept interfaces
type Mover interface {
	Move(x, y int)
	// Move(int, int)
}

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

func (p *Player) Found(key Key) error {
	switch key {
	case Copper, Jade, Crystal:
		// OK
	default:
		return fmt.Errorf("unknown key: %v", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}
	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if k == key {
			return true
		}
	}
	return false
}

/* What fmt.Print does
func Print(v any) {
	if s, ok := v.(fmt.Stringer); ok {
		print(s.String())
	}
	...
}
*/

// Implement fmt.Stringer interface
// See also golang.org/x/tools/cmd/stringer
func (k Key) String() string {
	switch k {
	case Jade:
		return "jade"
	case Copper:
		return "copper"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
)

/* Exercise:
- Add a Keys field which is a slice of strings to Player
- Add a Found(key string) method to Player
	- It should err they key is not one of "copper", "jade", "crystal"
	- It should add a key only once
*/

type Player struct {
	Name string
	Keys []Key
	Item // Player embeds Item
	// Xer
}

/*
type Xer struct {
	X string
}
*/

// i is called "the receiver"
// Use pointer receiver when mutating.
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

/*
func NewItem(x, y int) Item
func NewItem(x, y int) (Item, error)
func NewItem(x, y int) *Item
*/
func NewItem(x, y int) (*Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d out of bounds for %d/%d", x, y, maxX, maxY)
	}

	i := Item{
		X: x,
		Y: y,
	}
	// Go is doing escape analysis and will allocate i on the heap
	// go build -gcflags=-m
	return &i, nil
}

const (
	maxX = 1000
	maxY = 600
)

type Item struct {
	X int
	Y int
}
