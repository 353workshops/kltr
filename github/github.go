package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Millisecond)
	defer cancel()
	fmt.Println(userInfo(ctx, "tebeka")) // Miki Tebeka 64 <nil>
}

// Hint: fmt.Errorf
func userInfo(ctx context.Context, login string) (string, int, error) {
	// TODO: http.Escape
	url := "https://api.github.com/users/" + login
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	// resp, err := http.Get(url)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return "", 0, fmt.Errorf("bad status - %s", resp.Status)
	}

	var r Reply
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&r); err != nil {
		return "", 0, err
	}

	return r.Name, r.NumRepos, nil
}

func jsonDemo() {
	// GoLand: os.Open("github/response.json")
	file, err := os.Open("response.json")
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	var r Reply
	dec := json.NewDecoder(file)
	if err := dec.Decode(&r); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Printf("%#v\n", r)

	n := Num{7}
	inc(n)
	fmt.Println(n) // no change
	incPtr(&n)
	fmt.Println(n) // yes change
}

// map[string]any
type Reply struct {
	Name string
	// Public_Repos int
	NumRepos int `json:"public_repos"` // field tag
}

func incPtr(n *Num) {
	n.Val++
}

func inc(n Num) {
	n.Val++
}

type Num struct {
	Val int
}

func get() {
	url := "https://api.github.com/users/tebeka"
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("error: %s", err)
		// log.Printf + os.Exit(1)
		// log.Fatal* should be called only from main
	}
	// if status := resp.StatusCode; status != http.StatusOK {
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: bad status - %s", resp.Status)
	}

	// fmt.Println("ctype:", resp.Header.Get("Content-Type"))
	if _, err := io.Copy(os.Stdout, resp.Body); err != nil {
		log.Printf("error: can't get body - %s", err)
	}
}

/* JSON <-> Go
boolean <-> bool
string <-> string
null <-> nil
number <-> float64, float32, int, int8, ... int64, uint, ... uint64
array <-> []T, []any
object <-> struct, map[string]any
// MIA: time.Time

encoding/json API
JSON -> io.Reader -> Go: json.Decoder
Go -> io.Writer -> JSON: json.Encoder
JSON -> []byte -> Go: Unmarshal
Go -> []byte -> JSON: Marshal
*/
