package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
)

func main() {
	err := killServer("app.pid")
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			fmt.Println("> no such file")
		}
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println(">", e)
		}
		log.Fatalf("error: %s", err)
	}
}

func killServer(pidFile string) error {
	// read: os.Open
	// write: os.Create
	// append: os.OpenFile ...

	// idiom: acquire a resource, check for error, defer release
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("warning: can't close %q - %s", pidFile, err)
		}
	}()

	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		return fmt.Errorf("%q: bad pid - %w", pidFile, err)
	}

	// simulate kill
	fmt.Printf("killing: %d\n", pid)

	file.Close()
	if err := os.Remove(pidFile); err != nil {
		log.Printf("warning: can't delete %q - %s", pidFile, err)
	}

	return nil
}
