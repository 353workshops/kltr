package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

/*
What is the most common word in sherlock.txt (ignoring case)?

- Go over file one line at a time ✓
- Extract words from text (regular expression) ✓
- Count words (map) ✓
*/

var (
	// "Who's on first?" -> [Who s on first]
	wordRe = regexp.MustCompile(`[a-zA-Z]+`)
)

func main() {
	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	freq := make(map[string]int) // word -> count
	for s.Scan() {
		words := wordRe.FindAllString(s.Text(), -1)
		for _, w := range words {
			w = strings.ToLower(w)
			freq[w]++
		}
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	maxW, maxC := "", 0
	for w, c := range freq {
		if c > maxC {
			maxW, maxC = w, c
		}
	}
	fmt.Println(maxW)
}

func mapDemo() {
	/*
		type Symbol string
		type Price float64
		var stocks map[Symbol]Price
	*/
	var stocks map[string]float64 // symbol -> price
	fmt.Println(stocks)
	fmt.Println("len:", len(stocks))
	fmt.Println(stocks["KLTR"])
	price, ok := stocks["KLTR"]
	if !ok {
		fmt.Println("KLTR not found")
	} else {
		fmt.Printf("KLTR: %.2f\n", price)
	}

	// stocks["KLTR"] = 1.9 // will panic

	/*
		stocks = make(map[string]float64)
		stocks["KLTR"] = 1.9
	*/
	stocks = map[string]float64{
		"KLTR": 1.9,
		"IBM":  124.65,
	}
	price, ok = stocks["KLTR"]
	if !ok {
		fmt.Println("KLTR not found")
	} else {
		fmt.Printf("KLTR: %.2f\n", price)
	}

	for k := range stocks { // keys
		fmt.Println(k)
	}
	for k, v := range stocks { // keys + values
		fmt.Println(k, "->", v)
	}
}

func regexpDemo() {
	text := "Who's on first?"
	words := wordRe.FindAllString(text, -1)
	fmt.Println(words)
}

func lineDemo() {
	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	count := 0
	for s.Scan() {
		/*
			s.Text()
			s.Bytes()
		*/
		count++
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(count)

}

func init() {
	fmt.Println("init")
}
