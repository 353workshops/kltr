package main

import (
	"image/color"
	"log"
	"os"
)

var (
	Red   = color.RGBA{0xFF, 0, 0, 0xFF}
	Green = color.RGBA{0, 0xFF, 0, 0xFF}
	Blue  = color.RGBA{0, 0, 0xFF, 0xFF}
)

// TODO: Your code goes here

func main() {
	ic, err := NewImageCanvas(200, 200)
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	ic.Add(&Circle{100, 100, 80, Green})
	ic.Add(&Circle{60, 60, 10, Blue})
	ic.Add(&Circle{140, 60, 10, Blue})
	ic.Add(&Rectangle{100, 130, 80, 10, Red})

	file, err := os.Create("face.png")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	if err := ic.Draw(file); err != nil {
		log.Fatalf("error: %s", err)
	}
}
