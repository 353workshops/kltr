package main

import (
	"fmt"
	"sync"
)

func main() {
	p := Payment{
		From:   "Wile. E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}

	p.Execute()
	p.Execute()
}

func (p *Payment) Execute() {
	p.once.Do(p.execute)
}

func (p *Payment) execute() {
	fmt.Printf("%s -> $%.2f -> %s\n", p.From, p.Amount, p.To)
}

/*
func (p *Payment) Execute() {
	p.mu.Lock()
	defer p.mu.Unlock()

	if p.done {
		return
	}

	fmt.Printf("%s -> $%.2f -> %s\n", p.From, p.Amount, p.To)
	p.done = true
}
*/

type Payment struct {
	From   string
	To     string
	Amount float64 // $

	once sync.Once
	/*
		mu   sync.Mutex
		done bool
	*/
}
