# Practical Go for Developers

Kaltura ∴  2023 <br />

Miki Tebeka
<i class="far fa-envelope"></i> [miki@353solutions.com](mailto:miki@353solutions.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/)

- [Syllabus](_extra/syllabus.pdf)
- [Recordings](https://youtube.com/playlist?list=PL83a493xppqRwM1yBnRUAN8ZUVsNUHBEM)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/) book

---

[Final Exercise](_extra/dld.md)

---

## Day 1

### Agenda

- Strings & formatted output
    - What is a string?
    - Unicode basics
    - Using fmt package for formatted output
- Calling REST APIs
    - Making HTTP calls with net/http
    - Defining structs
    - Serializing JSON
- Working with files
    - Handling errors
    - Using defer to manage resources
    - Working with io.Reader & io.Writer interfaces

### Code

- [sha1.go](sha1/sha1.go) - `io.Reader` & `io.Writer` interfaces
- [kill_server.go](kill_server/kill_server.go) - Files, defer & errors
- [github.com](github/github.go) - Calling REST APIs
- [banner.go](banner/banner.go) - Working with strings & Unicode
- [hw.go](hw/hw.go) - Hello World

[Terminal Log](_extra/day-1.log)

### Exercise

The [NYC Taxi page](https://www.nyc.gov/site/tlc/about/tlc-trip-record-data.page) contains data files. For example the files for March 2021 are:
- `https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2021-03.parquet`
- `https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2021-03.parquet`

Write a function `downloadInfo(year int) error` that will print the file size of every file in the year and the total.
Use a HTTP `HEAD` request to get the content length.

For example, `downloadInfo(2021)` should print 25 lines:
```
https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2021-01.parquet 1333519
https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2021-01.parquet 21686067
https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2021-02.parquet 1145679
https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2021-02.parquet 21777258
...
total: 499826436
```


### Links

- [HTTP status cats](https://http.cat/)
- [errors](https://pkg.go.dev/errors/) package ([Go 1.13](https://go.dev/blog/go1.13-errors))
- [JSON: The Fine Print](https://archive.fosdem.org/2022/schedule/event/go_json/)
- [encoding/json](https://pkg.go.dev/encoding/json)
- [net/http](https://pkg.go.dev/net/http)
- Numbers
    - [math/big](https://pkg.go.dev/math/big/) - Big numbers
    - [Numeric types](https://go.dev/ref/spec#Numeric_types)
- Strings
    - [Unicode table](https://unicode-table.com/)
    - [strings](https://pkg.go.dev/strings/) package - string utilities
    - [Go strings](https://go.dev/blog/strings)
- [GoReleaser](https://goreleaser.com/) - Build for many platforms
- [Go Proverbs](https://go-proverbs.github.io/) - Think about them ☺
- [Annotated "Hello World"](https://www.353solutions.com/annotated-go)
- [Effective Go](https://go.dev/doc/effective_go.html) - Read this!
- [Go standard library](https://pkg.go.dev/) - official documentation
- [A Tour of Go](https://do.dev/tour/)
- Setting Up
    - [The Go SDK](https://go.dev/dl/)
    - [git](https://git-scm.com/)
    - IDE's: [Visual Studio Code](https://code.visualstudio.com/) + [Go extension](https://marketplace.visualstudio.com/items?itemName=golang.Go) or [Goland](https://www.jetbrains.com/go/) (paid)

### Data & Other

- `G☺`
- `♡`
- [http.log.gz](_extra/http.log.gz)
- `https://api.github.com/users/tebeka`
- [Slides](_extra/slides.pdf)
- [Unicode](_extra/unicode.pdf)

---

## Day 2

### Agenda

- Sorting
    - Working with slices
    - Writing methods
    - Understanding interfaces
- Catching panics
    - The built-in recover function
    - Named return values
- Processing text
    - Reading line by line with bufio.Scanner
    - Using regular expressions
    - Working with maps

### Code

- [freq.go](freq/freq.go) - Text processing, maps
- [empty.go](empty/empty.go) - The `any` or `interface{}`
- [stats.go](stats/stats.go) - Type constraints (generics)
- [game.go](game/game.go) - Structs, methods & interfaces
- [slices.go](slices/slices.go) - Working with slices

[Terminal Log](_extra/day-2.log)

### Exercises

#### 1. Paint

Implement a paining program. It should support

- Circle with location (x, y), color and radius
- Rectangle with location, color, width and height

Each shape should implement a `Draw(d Device)` method. Where `Device` is:

```go
type Device interface {
	Set(int, int, color.Color)
}
```

Implement an `ImageCanvas` struct which holds a slice of drawable items and has `Draw` methods that writes a PNG to an `io.Writer`. (using `image/png`)

```go
func (ic *ImageCanvas) Draw(w io.Writer) error {
    // ...
}
```

Implement your code in [draw.go](_extra/draw.go) and run it, it should produce the following image:  
![](_extra/face.png)


#### 2. Common Words

What are the 10 most common words in `shrelock.txt`?


```go
// MostCommon returns the "k" most common words (ignoring case) in r.
func MostCommon(r io.Reader, k int) ([]string, error) {
    ...
}
```

### Links

- [Hyrum's Law](https://www.hyrumslaw.com/)
- [regex101](https://regex101.com/) - Regular expression builder
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour
- Slices
    - [Slices](https://go.dev/blog/slices) & [Slice internals](https://go.dev/blog/go-slices-usage-and-internals) on the Go blog
    - [Slice tricks](https://github.com/golang/go/wiki/SliceTricks)
- Error Handling
    - [Defer, Panic and Recover](https://go.dev/blog/defer-panic-and-recover)
    - [errors](https://pkg.go.dev/errors/) package ([Go 1.13](https://go.dev/blog/go1.13-errors))
    - [pkg/errors](https://github.com/pkg/errors)

### Data & Other

- [sherlock.txt](_extra/sherlock.txt)
- [slices](_extra/slices.md)
- [Method sets](_extra/method-sets.pdf)

---

## Day 3

### Agenda

- Distributing work
    - Using goroutines & channels
    - Using the sync package to coordinate work
- Timeouts & cancellation
    - Working with multiple channels using select
    - Using context for timeouts & cancellations
    - Standard library support for context

### Code

- [rtb.go](rtb/rtb.go) - Context
- [counter.go](counter/counter.go) - Using the race detector, `sync.Mutex` and `sync/atomic`
- [payment.go](payment/payment.go) - Using sync.Once
- [url_check.go](url_check/url_check.go) - Fan out & sync.WaitGroup
- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
    - [sleep_sort.sh](go_chan/sleep_sort.sh)

[Terminal Log](_extra/day-3.log)

### Exercise

Extract [taxi-sha256.zip](https://storage.googleapis.com/353solutions/c/data/taxi-sha256.zip) to a local folder.
Run [taxi_check.go](_extra/taxi_check.go) and note the run time (you might need to change `rootDir` to where you extracted `taxi-sha256.zip`).
    - There is an error in file 6 - it is expected

Change the code to run with goroutine per file, how much faster did the code get?

Bonus: Limit the number of goroutines to `k` (say 8), what is the speed difference?


### Links

- [In the Loop](https://www.youtube.com/watch?v=9dXGNWsFb0Q) The many ways to loop in Go
- [The race detector](https://go.dev/doc/articles/race_detector)
- [Uber Go Style Guide](https://github.com/uber-go/guide/blob/master/style.md)
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup)
- [Data Race Patterns in Go](https://eng.uber.com/data-race-patterns-in-go/)
- [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
- [Go Concurrency Patterns: Context](https://go.dev/blog/context)
- [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
- [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
- [Channel Semantics](https://www.353solutions.com/channel-semantics)
- [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
- [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy

### Data & Other

- [rtb.go](_extra/rtb.go)
- [site_times.go](_extra/site_time.go)
- [taxi_check.go](_extra/taxi_check.go)
    - [taxi-sha256.zip](https://storage.googleapis.com/353solutions/c/data/taxi-sha256.zip)


---

## Day 4

### Agenda

- Testing your code
    - Working with the testing package
    - Using testify
    - Managing dependencies with go mod
- Structuring your code
    - Writing sub-packages
- gRPC server & client
    - Writing protocol buffers definitions
    - Implementing server & client
Adding metrics & logging
    - Using expvar for metrics
    - Using the log package and a look at user/zap
- Configuration patterns
    - Reading environment variables and a look at external packages
    - Using the flag package for command line processing

### Code

`nlp` project


### Exercise

[Parallel File Downloader](_extra/dld.md)

[Terminal Log](_extra/day-4.log)

### Links

- [Conway's Law](https://martinfowler.com/bliki/ConwaysLaw.html)
- [The Twelve-Factor App](https://12factor.net)
- Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
- Logging 
    - Built-in [log](https://pkg.go.dev/log/)
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
    - The experimental [slog](https://pkg.go.dev/golang.org/x/exp/slog)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- gRPC
    - [Writing gRPC interceptors in Go](https://shijuvar.medium.com/writing-grpc-interceptors-in-go-bf3e7671fe48)
    - [Go gRPC Middleware](https://github.com/grpc-ecosystem/go-grpc-middleware)
    - [gRPC metadata](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md)
    - [gRPC status codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
    - [gRPCurl](https://github.com/fullstorydev/grpcurl)
        - [More gRPC tools](https://github.com/grpc-ecosystem/awesome-grpc#tools)
    - [Go protobuf docs](https://pkg.go.dev/google.golang.org/protobuf)
    - [gRPC in Go](https://grpc.io/docs/languages/go/quickstart/)
- HTTP Servers
    - [net/http](https://pkg.go.dev/net/http/)
    - [net/http/httptest](https://pkg.go.dev/net/http/httptest)
    - [chi](https://github.com/go-chi/chi) - A nice web framework
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
- [Example Project Structure](https://github.com/ardanlabs/service)
- [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Go Doc Comments](https://go.dev/doc/comment)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18+)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/github.com/353solutions/nlp)
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [vulncheck](https://pkg.go.dev/golang.org/x/vuln/vulncheck) - Check for CVEs
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- Testing
    - [testing](https://pkg.go.dev/testing/)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify) - Many test utilities (including suites & mocking)
    - [Ginkgo](https://onsi.github.io/ginkgo/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
        - [testing/quick](https://pkg.go.dev/testing/quick) - Initial fuzzing library
    - [test containers](https://golang.testcontainers.org/)

### Data & Other

- [nlp.go](_extra/nlp.go)
- [stemmer.go](_extra/stemmer.go)
- [tokenize_cases.yml](_extra/tokenize_cases.yml)

---
Copyright © [353solutions](https://www.353solutions.com). **All rights reserved, DO NOT COPY!**
