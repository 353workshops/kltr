package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	/* Fix 2: Local loop variable */
	for i := 0; i < 3; i++ {
		i := i // i shadows i from line 14
		go func() {
			fmt.Println(i) // i from line 15
		}()
	}

	/* Fix 1: Pass a parameter
	for i := 0; i < 3; i++ {
		go func(n int) {
			fmt.Println(n)
		}(i)
	}
	*/
	/*
		for i := 0; i < 3; i++ {
			go func() {
				fmt.Println(i) // BUG: All goroutines use the same i
			}()
		}
	*/

	time.Sleep(10 * time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 42 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	values := []int{20, 30, 10}
	fmt.Println(sleepSort(values)) // [10 20 30]

	go func() {
		for i := 0; i < 5; i++ {
			ch <- i
		}
		close(ch)
	}()

	for n := range ch {
		fmt.Println("n:", n)
	}

	/* What the above "for n := range ch {" does
	for {
		n, ok := <-ch
		if !ok {
			break
		}
		fmt.Println("n:", n)
	}
	*/

	n := <-ch
	fmt.Println("n (closed):", n)

	n, ok := <-ch
	fmt.Println("n (closed):", n, "ok:", ok)
	// ch <- 7 // send to a closed channel (panic)
	// close(ch) // closing a closed channel (panic)

	fmt.Println("wait for it...")
	signal := make(chan struct{})
	for _, name := range []string{"a", "b", "c"} {
		go func(id string) {
			fmt.Printf("%s waits\n", id)
			<-signal
			fmt.Printf("%s starts\n", id)
		}(name)
	}
	time.Sleep(10 * time.Millisecond)

	fmt.Println("go!")
	close(signal)
	time.Sleep(10 * time.Millisecond)

	ch1, ch2 := make(chan string), make(chan string)

	/*
		fn1 := func() {
			time.Sleep(20 * time.Millisecond)
			ch1 <- "one"
		}
		go fn1()

		fn2 := func() {
			time.Sleep(10 * time.Millisecond)
			ch2 <- "two"
		}
		go fn2()
	*/
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch1 <- "one"
	}()

	go func() {
		time.Sleep(10 * time.Millisecond)
		ch2 <- "two"
	}()

	/*
		done := make(chan struct{})
		go func() {
			time.Sleep(100 * time.Millisecond)
			close(done)
		}()
	*/

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Printf("Got %#v from ch1\n", val)
	case val := <-ch2:
		fmt.Printf("Got %#v from ch2\n", val)
	// case <-time.After(5 * time.Millisecond):
	// case <-done:
	case <-ctx.Done():
		fmt.Println("timeout")
	}
}

/* Channel semantics
- send/receive will block until opposite operation(*)
	- buffered channel with capacity "n" has "n" non-blocking sends
- receive from a closed channel will return zero value without blocking
	- Use "v, ok := <- ch" to check (ok=false means channel is closed)
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/

/*
sleep sort:
for every value n in values, spin a goroutine that will
- sleep n milliseconds
- send n over a channel

collect all values from the channel into a slice and return it.
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	// fan out
	for _, n := range values {
		n := n
		go func() {
			// time.Sleep(n * time.Millisecond)
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var out []int
	// out := make([]int, 0, len(values))
	for range values {
		n := <-ch
		out = append(out, n)
	}
	return out
}
