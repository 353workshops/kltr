package main

import "fmt"

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
	fmt.Println("fin")
}

func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables here (like a & b)

	defer func() {
		if e := recover(); e != nil {
			// fmt.Println("ERROR:", e)
			err = fmt.Errorf("%s", e)
		}
	}()

	// What usually inside the "try" block (happy path)
	return div(a, b), nil

	/* Miki don't like this style
	q = div(a, b)
	return
	*/
}

// external code, can't be changed
func div(a, b int) int {
	return a / b
}
