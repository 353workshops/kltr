package main

import (
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"
)

func main() {
	urls := []string{
		"https://go.dev",
		"https://pkg.go.dev",
		"https://corp.kaltura.com",
	}

	// withChannel(urls)

	start := time.Now()
	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() {
			defer wg.Done()

			d, n, err := urlCheck(url)
			fmt.Printf("%q: %d bytes in %v (error: %v)\n", url, n, d, err)
		}()
	}

	wg.Wait() // block until all done
	duration := time.Since(start)
	fmt.Printf("%d sites in %v\n", len(urls), duration)
}

func withChannel(urls []string) {
	start := time.Now()
	ch := make(chan result)
	// fan out
	for _, url := range urls {
		// d, err := urlCheck(url)
		go worker(url, ch)
	}

	// collect results

	// for i := 0; i < len(urls); i++ {
	for range urls {
		r := <-ch
		fmt.Printf("%q: %d bytes in %v (error: %v)\n", r.url, r.n, r.d, r.err)
	}

	duration := time.Since(start)
	fmt.Printf("%d sites in %v\n", len(urls), duration)
}

func worker(url string, ch chan result) {
	r := result{
		url: url,
	}

	defer func() {
		if e := recover(); e != nil {
			r.err = fmt.Errorf("%v", e)
			ch <- r
		}

	}()

	r.d, r.n, r.err = urlCheck(url)
	ch <- r
}

type result struct {
	url string // call context
	d   time.Duration
	n   int64
	err error
}

func urlCheck(url string) (time.Duration, int64, error) {
	start := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		return 0, 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return 0, 0, fmt.Errorf("%q: bad status - %s", url, resp.Status)
	}
	n, err := io.Copy(io.Discard, resp.Body)
	if err != nil {
		return 0, 0, err
	}

	return time.Since(start), n, nil
}
