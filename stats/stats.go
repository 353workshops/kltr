package main

import "fmt"

func main() {
	fmt.Println(Max([]int{3, 1, 2}))
	fmt.Println(Max[int](nil))
	fmt.Println(Max([]float64{3, 1, 2}))
	fmt.Println(Max([]string{"B", "C", "A"}))
}

type Ordered interface {
	int | float64 | string
}

// type constraint inside []
func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

/*
func MaxFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

func MaxInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

*/
