package main

import (
	"fmt"
	"sort"
)

func main() {
	var s1 []int // s1 is a slice of integers
	fmt.Println(s1)
	fmt.Println("len:", len(s1))

	s1 = []int{10, 20, 30, 40, 50, 60}
	fmt.Println(s1)
	fmt.Println("len:", len(s1))
	fmt.Println("s1[2]:", s1[2])

	for i := range s1 { // indices
		fmt.Println(i)
	}

	for i, v := range s1 { // indices + values
		fmt.Println(i, v)
	}
	n := -1
	s1[2] = n
	fmt.Println("s1:", s1)

	i := 2
	s2 := s1[i:4] // slicing notation, half-open range
	fmt.Println("s2:", s2)
	s2[0] = 99
	fmt.Println("s2:", s2)
	fmt.Println("s1:", s1)

	s1 = append(s1, 70)
	fmt.Println("s1:", s1)

	var s3 []int
	for i := 0; i < 1000; i++ {
		s3 = appendInt(s3, i)
	}
	fmt.Println("s3:", s3[:10], "len:", len(s3), "cap:", cap(s3))

	fmt.Println(concat([]string{"A", "B"}, []string{"C"})) // [A B C]

	vals := []float64{3, 1, 2}
	fmt.Println(median(vals)) // 2 <nil>
	vals = []float64{3, 1, 2, 4}
	fmt.Println(median(vals)) // 2.5 <nil>
	fmt.Println("vals:", vals)

	nums := []int{1, 2, 3}
	// value semantics
	for _, v := range nums {
		v++
	}
	fmt.Println("nums:", nums)

	// pointer(ish) semantics
	// for i := 0; i < len(nums); i++ {
	for i := range nums {
		nums[i]++
	}
	fmt.Println("nums:", nums)
}

func median(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("median of empty slice")
	}

	// Copy in order not to changes values in sort.
	vals := make([]float64, len(values))
	copy(vals, values)

	// Show memory location (value of address field)
	fmt.Printf("vals: %p, values: %p", &vals[0], &values[0])

	sort.Float64s(vals)
	i := len(vals) / 2
	if len(vals)%2 == 1 {
		return vals[i], nil
	}

	m := (vals[i-1] + vals[i]) / 2
	return m, nil
}

func concat(s1, s2 []string) []string {
	s := make([]string, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func appendInt(s []int, v int) []int {
	i := len(s)
	if len(s) < cap(s) { // there's space in the underlying array
		s = s[:len(s)+1]
	} else { // need to re-allocate and copy
		size := 2 * (len(s) + 1)
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[i] = v
	return s
}
