package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/*
		var mu sync.Mutex
		count := 0
	*/
	count := int64(0)

	var wg sync.WaitGroup
	const n = 10
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			defer wg.Done()
			for i := 0; i < 1000; i++ {
				/*
					mu.Lock()
					count++ // race condition
					mu.Unlock()
				*/
				atomic.AddInt64(&count, 1)
				time.Sleep(time.Microsecond)
			}
		}()
	}

	wg.Wait()
	fmt.Println(count)
}

// go test -race
// go run -race
// go build -race
