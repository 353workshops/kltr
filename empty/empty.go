package main

import "fmt"

func main() {
	// var i interface{} // go < 1.18
	var i any

	// Rule of thumb: Don't use any.
	i = 7
	fmt.Println(i)
	// i++ // won't compile

	i = "Hi"
	fmt.Printf("%v (%T)\n", i, i)

	s := i.(string) // type assertion
	fmt.Println("s:", s)

	// n := i.(int) // will panic
	n, ok := i.(int) // comma, ok
	if ok {
		fmt.Println("n:", n)
	} else {
		fmt.Println("not an int")
	}
}
