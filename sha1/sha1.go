package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
}

// exercise: decompress the file only if it ends with .gz
// cat sha1.go | sha1sum
// Hint: strings.HasSuffix

// cat http.log.gz | gunzip | sha1sum
// ef7dc39754fd23f7a1a8657e2ffda49edc49fff9
func fileSHA1(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file
	if strings.HasSuffix(fileName, ".gz") {
		r, err = gzip.NewReader(file)
		// r, err := gzip.NewReader(file) // BUG: New variable "r"
		if err != nil {
			return "", err
		}
	}

	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
