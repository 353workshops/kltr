package main

import (
	"context"
	"expvar"
	"log"
	"net"
	"net/http"
	"os"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"github.com/353solutions/nlp"
	"github.com/353solutions/nlp/pb"
)

func metricsServer() {
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Printf("error: can't run HTTP on :8080 - %s", err)
	}
	// TODO: Should we crash? continue?
	os.Exit(1)
}

var (
	tokCalls  = expvar.NewInt("tokenize.calls")
	tokErrors = expvar.NewInt("tokenize.errors")
)

func main() {
	/* Using TLS
	creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
	srv := grpc.NewServer(grpc.Creds(creds))
	*/

	var logger *zap.Logger
	var err error

	if os.Getenv("ENV") == "dev" {
		logger, err = zap.NewDevelopment()
	} else {
		logger, err = zap.NewProduction()
	}

	if err != nil {
		log.Fatalf("error: can't create logger - %s", err)
	}
	defer logger.Sync()

	n := NLP{
		log: logger.Sugar(),
	}

	// log.Printf("info: exposing metrics on :8080")
	n.log.Infow(
		"metrics server starting",
		"address", ":8080",
	)
	go metricsServer()

	srv := grpc.NewServer()
	pb.RegisterNLPServer(srv, &n)
	reflection.Register(srv)

	addr := ":8081"
	// log.Printf("info: server starts on %s", addr)
	n.log.Infow(
		"gRPC server starting",
		"address", addr,
	)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	if err := srv.Serve(lis); err != nil {
		log.Fatalf("error: %s", err)
	}
}

func (n *NLP) Tokenize(ctx context.Context, req *pb.TokenizeRequest) (*pb.TokenizeResponse, error) {
	tokCalls.Add(1)
	// Validate
	if req.Text == "" {
		n.log.Error("empty request")
		tokErrors.Add(1)
		return nil, status.Errorf(codes.InvalidArgument, "empty text")
	}

	tokens := nlp.Tokenize(req.Text)

	resp := pb.TokenizeResponse{
		Tokens: tokens,
	}
	return &resp, nil
}

type NLP struct {
	pb.UnimplementedNLPServer

	log *zap.SugaredLogger
}

/*
Configuration: defaults < config file < environment < command line
defaults: code
config file: yaml or toml
environment: os.Getenv
command line: flag

Also:
- viper + cobra
- ardanlabs/conf
*/
