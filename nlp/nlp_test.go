package nlp

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

// A -> C@1.2
// B -> C@1.4

func TestInCI(t *testing.T) {
	// In Jenkins use BUILD_NUMBER
	if os.Getenv("CI") == "" {
		t.Skip("not in CI")
	}

	t.Log("running test")
}

/*
Exercise: Read test cases from testdata/tokenize_cases.yml instead of
in memory testCases slice.

- You'll need to import gopkg.in/yaml.v3
- You'll need a TestCase type
  - All fields must be exported (upper cases)
*/
type TokCase struct {
	Text   string
	Tokens []string
	Name   string
}

// func loadTokCases(t *testing.T) ([]TokCase, error) {

func loadTokCases(t *testing.T) []TokCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err)
	defer file.Close()

	var tcs []TokCase
	err = yaml.NewDecoder(file).Decode(&tcs)
	require.NoError(t, err)
	return tcs
}

func TestTokenizeTable(t *testing.T) {
	/*
		var testCases = []struct {
			text   string
			tokens []string
		}{
			{"", nil},
			{"What's on second?", []string{"what", "s", "on", "second"}},
		}
		for _, tc := range testCases {
	*/

	for _, tc := range loadTokCases(t) {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}

		t.Run(name, func(t *testing.T) {
			t.Logf("text: %q", tc.Text)
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Tokens, tokens)
			/* before require
			if !reflect.DeepEqual(tc.tokens, tokens) {
				t.Fatalf("expected: %#v, got %#v", tc.tokens, tokens)
			}
			*/
		})
	}
}

func setup(t *testing.T) {
	// TODO
	// Option 2 (without defer)
	t.Cleanup(teardown)
}

func teardown() {
	// TODO
}

func TestTokenize(t *testing.T) {
	setup(t)
	defer teardown()

	text := "Who's on first?"
	tokens := Tokenize(text)
	expected := []string{"who", "on", "first"}
	require.Equal(t, expected, tokens)
	// if tokens != expected { // can't compare slices
	/* Before testify
	if !reflect.DeepEqual(expected, tokens) {
		t.Fatalf("expected: %#v, got %#v", expected, tokens)
	}
	*/
}
